using UnityEngine;

public class ExplodeCubes : MonoBehaviour
{
    private bool _collisionSet;
    public GameObject _restartButton;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Cube" && !_collisionSet)
        {
            for (int i = collision.transform.childCount - 1; i >= 0; i--)
            {
                Transform child = collision.transform.GetChild(i);
                child.gameObject.AddComponent<Rigidbody>();
                child.gameObject.GetComponent<Rigidbody>().AddExplosionForce(70f, Vector3.up, 5f);
                child.SetParent(null);
            }

            _restartButton.SetActive(true);

            Camera.main.transform.position = Vector3.MoveTowards(
                Camera.main.transform.position,
                new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z - 3f),
                2f * Time.deltaTime);

            //Camera.main.transform.position -= new Vector3(0,0,3);
            Camera.main.gameObject.AddComponent<CameraShake>();
            Destroy(collision.gameObject);
            _collisionSet = true;
        }
    }
}
