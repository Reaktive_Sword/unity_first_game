using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour {
    private CubePos nowCube = new CubePos(0,1,0);
    public float cubeChangePlaceSpeed = 0.5f;
    public Transform cubeToPlace;
    private float camMoveToYPosition;
    public GameObject cubeToCreate, allCubes;
    private Rigidbody allCubesRB;
    private Transform mainCam;
    private float camMoveSpeed = 2f;
    private bool isLose;
    private bool firstCube;
    private int prevCountMaxHorizontal = 0;
    public Color[] bgColors;
    private Color toCameraColor;

    public GameObject[] canvasStartPage;

    private Coroutine _showCubePlace;

    private List<Vector3> allCubesPositions = new List<Vector3>
    {
        new Vector3(0,0,0),
        new Vector3(1,0,0),
        new Vector3(-1,0,0),
        new Vector3(0,1,0),
        new Vector3(0,0,1),
        new Vector3(0,0,-1),
        new Vector3(1,0,1),
        new Vector3(-1,0,-1),
        new Vector3(-1,0,1),
        new Vector3(1,0,-1),
    };

    private void Start() {
        mainCam = Camera.main.transform;
        toCameraColor = Camera.main.backgroundColor;
        camMoveToYPosition = 5.9f + nowCube.y - 1f;

        allCubesRB = allCubes.GetComponent<Rigidbody>();
        _showCubePlace = StartCoroutine(ShowCubePlace());
    }

    private void Update()
    {
        if (cubeToPlace == null)
        {
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (allCubes == null)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) || Input.touchCount > 0)
        {
#if !UNITY_EDITOR
            if (Input.GetTouch(0).phase != TouchPhase.Began)
            {
                return
            }
#endif      

            if (!firstCube)
            {
                firstCube = true;
                foreach (var item in canvasStartPage)
                {
                    Destroy(item);
                }
            }

            GameObject newCube = Instantiate(cubeToCreate, cubeToPlace.position, Quaternion.identity) as GameObject;

            newCube.transform.SetParent(allCubes.transform);
            nowCube.setVector(cubeToPlace.position);
            allCubesPositions.Add(nowCube.getVector());

            allCubesRB.isKinematic = true;
            allCubesRB.isKinematic = false;

            SpawnPositions();
            MoveCameChangeBG();
        }

        if (!isLose && allCubesRB.velocity.magnitude > 0.1)
        {
            Destroy(cubeToPlace.gameObject);
            isLose = true;
            StopCoroutine(_showCubePlace);
        }

        mainCam.localPosition = Vector3.MoveTowards(
            mainCam.localPosition,
            new Vector3(mainCam.localPosition.x, camMoveToYPosition, mainCam.localPosition.z),
            camMoveSpeed * Time.deltaTime);

        if (Camera.main.backgroundColor != toCameraColor)
        {
            Camera.main.backgroundColor = Color.Lerp(
                Camera.main.backgroundColor,
                toCameraColor,
                Time.deltaTime / 1.5f);
        }
    }

    IEnumerator ShowCubePlace()
    {
        while (true)
        {
            SpawnPositions();

            yield return new WaitForSeconds(cubeChangePlaceSpeed);
        }
    }

    private void SpawnPositions()
    {
        List<Vector3> positions = new List<Vector3>();
        if (isPositionEmpty(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z)) && nowCube.x + 1 != cubeToPlace.position.x)
        {
            positions.Add(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z));
        }
        if (isPositionEmpty(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z)) && nowCube.x - 1 != cubeToPlace.position.x)
        {
            positions.Add(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z));
        }
        if (isPositionEmpty(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z)) && nowCube.y + 1 != cubeToPlace.position.y)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z));
        }
        if (isPositionEmpty(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z)) && nowCube.y - 1 != cubeToPlace.position.y)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z));
        }
        if (isPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1)) && nowCube.z + 1 != cubeToPlace.position.z)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1));
        }
        if (isPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1)) && nowCube.z - 1 != cubeToPlace.position.y)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1));
        }

        if (positions.Count > 1)
            cubeToPlace.position = positions[UnityEngine.Random.Range(0, positions.Count)];
        else if (positions.Count == 0)
            isLose = true;
        else
            cubeToPlace.position = positions[0];
    }

    private bool isPositionEmpty(Vector3 pos)
    {
        if (pos.y == 0)
            return false;

        if (allCubesPositions.Any(p => p.x == pos.x && p.y == pos.y && p.z == pos.z))
            return false;

        return true;
    }

    private void MoveCameChangeBG()
    {
        int maxX = (int)allCubesPositions.Select(x => Math.Abs(x.x)).Max(x => x);
        int maxY = (int)allCubesPositions.Select(x => Math.Abs(x.y)).Max(x => x);
        int maxZ = (int)allCubesPositions.Select(x => Math.Abs(x.z)).Max(x => x);
        int maxHor = Math.Max(maxX, maxZ);

        camMoveToYPosition = 5.9f + nowCube.y - 1f;

        if (maxHor % 3 == 0 && prevCountMaxHorizontal != maxHor)
        {
            mainCam.localPosition += new Vector3(0, 0, -2.5f);
            prevCountMaxHorizontal = maxHor;
        }

        if (maxY >= 7)
            toCameraColor = bgColors[2];
        else if (maxY >= 5)
            toCameraColor = bgColors[1];
        else if (maxY >= 2)
            toCameraColor = bgColors[0];
    }
}

struct CubePos {
    public int x, y, z;

    public CubePos(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3 getVector() {
        return new Vector3(x, y, z);
    }

    public void setVector(Vector3 pos)
    {
        x = Convert.ToInt32(pos.x);
        y = Convert.ToInt32(pos.y);
        z = Convert.ToInt32(pos.z);
    }
}
